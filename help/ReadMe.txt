* Add new topics 
* modify toc.xml, index.xml and map.xml as appropriate
* use jhindexer to refresh keyword search database
* use jar command to create help.jar file (zipping doesn't work - *shrug*)

Search Indexer: something like...
(from within help directory, assuming projects/javahelp is set up...)
 ../../javahelp/javahelp/bin/jhindexer . -verbose

(creates /JavaSearchHelp, referenced in JXplorerHelp.hs)

Jar command:
* e.g. (from within help directory) >jar -cvf help.jar *


TODO: make ant task?
(use makehelp.sh for the time being...)

NOTE: for bonus points, jhindexer fails every second time; something to do with creating the Subdirectories such as DOC?
(In the meantime, run it twice...)